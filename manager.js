var EventEmitter = require('events').EventEmitter;
var util = require('util');
var Process = require('./process');
var _ = require('lodash');

function Manager() {
    var self = this;
    this.lastUpdate = Date.now();
    this.nodes = [];
    this.lines = [];

    EventEmitter.call(this);

    Object.defineProperty(this, "response", {
        get: function() {
            return {nodes: this.nodes, lines: this.lines, lastUpdate: this.lastUpdate};
        }
    });
}
util.inherits(Manager, EventEmitter);

module.exports = Manager;


Manager.prototype.add = function(options) {
    var self = this;

    if (typeof (options) === 'string') {
        options = {
            node: {
                mac: options
            }
        };
    }

    this.lastUpdate = options.lastUpdate || Date.now();

    if (_.has(options, 'node')) {
        var nodes = _.concat([], options.node);
        _.forEach(nodes, function(node) {
            self.addNode(node);
        });
    }

    if (_.has(options, 'line')) {
        var lines = _.concat([], options.line);
        _.forEach(lines, function(line) {
            self.addLine(line);
        });
    }

    return (this);
}

Manager.prototype.addNode = function addNode(node) {
    var self = this;
    if (typeof (node) === 'string') {
        node = {
            mac: node
        };
    }

    if (!_.isObject(node)) return (this);
    if (!_.has(node, 'mac')) return (this);
    if (-1 !== _.findIndex(this.nodes, ['mac', node.mac])) return (this);

    var child = new Process(node);
    this.nodes.push(child);
    child.process.on('close', removeItself);

    function removeItself(code, signal) {
        var removed = _.remove(self.nodes, function(node) {
            return child.mac === node.mac;
        });
        if (!_.isEmpty(removed)) {
            self.emit('remove.node', removed);
        }
    }

    this.emit('new.node', child);

    return (this);
}

Manager.prototype.addLine = function addLine(line) {
    var self = this;

    if (!_.isObject(line)) return (this);
    if (!_.has(line, 'source.mac')) return (this);
    if (!_.has(line, 'target.mac')) return (this);
    if (!_.every(this.lines, checkLine)) return (this);
    function checkLine(existing) {
        function checkCase(a, b) {
            return (
                (existing.source.mac === a) &&
                (existing.target.mac === b)
            );
        }
        return !(
            checkCase(line.source.mac, line.target.mac) ||
            checkCase(line.target.mac, line.source.mac)
        );
    }

    var idxSource = _.findIndex(this.nodes, ['mac', line.source.mac]);
    var idxTarget = _.findIndex(this.nodes, ['mac', line.target.mac]);
    if (-1 == idxSource) return (this);
    if (-1 == idxTarget) return (this);

    _.merge(line.source, _.pick(this.nodes[idxSource], ['x', 'y']));
    _.merge(line.target, _.pick(this.nodes[idxTarget], ['x', 'y']));

    this.lines = _.concat(this.lines, line);

    this.emit('new.line', line, this);

    return (this);

}






Manager.prototype.change = function(options) {
    var self = this;
    if (typeof (options) === 'string') {
        options = {
            node: {
                mac: options
            }
        };
    }

    this.lastUpdate = options.lastUpdate || Date.now();

    if (_.has(options, 'node')) {
        var nodes = _.concat([], options.node);
        _.forEach(nodes, function(node) {
            self.changeNode(node);
        });
    }

    if (_.has(options, 'line')) {
        var lines = _.concat([], options.line);
        _.forEach(lines, function(line) {
            self.changeLine(line);
        });
    }

    return (this);
}

Manager.prototype.changeNode = function changeNode(node) {
    var self = this;

    if (!_.isObject(node)) return (this);
    if (!_.has(node, 'mac')) return (this);

    // TODO: Flatten node

    var idxNode = _.findIndex(this.nodes, ['mac', node.mac]);
    if (-1 == idxNode) return (this);

    node = _.merge(this.nodes[idxNode], node);

    // TODO: changeLine()
    _.forEach(this.lines, function(line) {
        if (line.source.mac === node.mac) {
            _.merge(line.source, _.pick(node, ['x', 'y']));
            self.emit('update.line', line);
        }
        if (line.target.mac === node.mac) {
            _.merge(line.target, _.pick(node, ['x', 'y']));
            self.emit('update.line', line);
        }
    });

    this.emit('update.node', node);

    return (this);
}

Manager.prototype.changeLine = function changeLine(line) {
    // TODO: Method
    return (this);
}




Manager.prototype.remove = function(options) {
    var self = this;

    if (typeof (options) === 'string') {
        options = {
            node: {
                mac: options
            }
        };
    }

    this.lastUpdate = options.lastUpdate || Date.now();

    if (_.has(options, 'node')) {
        var nodes = _.concat([], options.node);
        _.forEach(nodes, function(node) {
            self.removeNode(node);
        });
    }

    if (_.has(options, 'line')) {
        var lines = _.concat([], options.line);
        _.forEach(lines, function(line) {
            self.removeLine(line);
        });
    }

    return (this);
}


Manager.prototype.removeNode = function removeNode(node) {
    var self = this;

    if (!_.isObject(node)) return (this);
    if (!_.has(node, 'mac')) return (this);

    var idxNode = _.findIndex(this.nodes, ['mac', node.mac]);
    if (-1 == idxNode) return (this);

    // The node will remove itself and fire event
    var _node = this.nodes[idxNode];
    _node.process.kill();

    // TODO: removeLine()
    var removed = _.remove(this.lines, function(n) {
        return (n.source.mac === node.mac) || (n.target.mac === node.mac);
    });

    if (!_.isEmpty(removed)) {
        this.emit('remove.line', removed, this);
    }


    return (this);
}

Manager.prototype.removeLine = function removeLine(line) {
    var self = this;

    if (!_.isObject(line)) return (this);
    if (!_.has(line, 'source.mac')) return (this);
    if (!_.has(line, 'target.mac')) return (this);

    var removed = _.remove(this.lines, function(existing) {
        function removeCase(a, b) {
            return (
                (existing.source.mac === a) &&
                (existing.target.mac === b)
            );
        }
        return (
            removeCase(line.source.mac, line.target.mac) ||
            removeCase(line.target.mac, line.source.mac)
        );
    });

    if (!_.isEmpty(removed)) {
        this.emit('remove.line', removed, this);
    }

    return (this);
}




//Manager.prototype.updateLines = function(newLines) {
//    var self = this;
//    var updatedLines = _.concat([], newLines);
//    var diff = _.differenceWith(this.lines, updatedLines, function(arrVal, othVal) {
//        return (arrVal.source.mac == othVal.source.mac) &&
//            (arrVal.target.mac == othVal.target.mac);
//    });
//
//    _.forEach(updatedLines, function(line) {
//        var wrap = _.wrap(_.padStart, function(func, text) {
//            return '_0x' + func(text, 4, '0') + '_';
//        });
//
//        var s = _.chain(self.getChannelNames(line.source.mac))
//            .map(function(item) {
//                return wrap(item);
//            })
//            .join('')
//            .value();
//        var t = _.chain(self.getChannelNames(line.target.mac))
//            .map(function(item) {
//                return wrap(item);
//            })
//            .join('')
//            .value();
//
//        console.log(wrap(line.source.mac), s);
//        console.log(wrap(line.target.mac), t);
//    });
//
//    return _.concat(diff, updatedLines);
//}
//
//Manager.prototype.getChannelNames = function(mac) {
//    var scope = [];
//    var node = _.chain(this.nodes).filter(['mac', mac]).head().pick('mac').value();
//
//    if (_.isEmpty(node)) return scope;
//
//    _.forEach(this.lines, function(line) {
//        if (line.source.mac === node.mac) {
//            scope.push(line.target.mac);
//        }
//        if (line.target.mac === node.mac) {
//            scope.push(line.source.mac);
//        }
//    });
//
//    return scope;
//}

