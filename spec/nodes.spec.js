var manager = require('../index');
var _ = require('lodash');


describe("Test Nodes:", function() {
    var path = 'I:\\!Диссер\\emb6server\\exec\\emb6blank.exe';

    var nodes = 0;

    afterAll(function(done) {
        _.forEach(manager.nodes, function(node) {
            manager.remove({node: node});
        });
        setTimeout(function() {
            done();
        }, 100);
    });

    it("add new node (a)", function() {
        manager.add({ node: { mac: 'a', path: path } });
        expect(manager.nodes.length).toEqual(++nodes);
    });

    it("add new nodes (b, c)", function() {
        var nodes_arr = [];
        nodes_arr.push({ mac: 'b', path: path });
        nodes_arr.push({ mac: 'c', path: path });
        manager.add({ node: nodes_arr });
        expect(manager.nodes.length).toEqual(nodes += 2);
    });

    it("add existing node (a)", function() {
        manager.add({ node: { mac: 'a', path: path } });
        expect(manager.nodes.length).toEqual(nodes);
    });

    it("remove node (a)", function(done) {
        var node = { mac: 'a' };
        manager.remove({ node: node });
        setTimeout(function() {
            expect(manager.nodes.length).toEqual(--nodes);
            done();
        }, 100);
    });

    it("add existing node (b)", function() {
        manager.add({ node: { mac: 'b', path: path } });
        expect(manager.nodes.length).toEqual(nodes);
    });

    it("add new node (a) and existing node (c)", function() {
        var nodes_arr = [];
        nodes_arr.push({ mac: 'a', path: path });
        nodes_arr.push({ mac: 'c', path: path });
        manager.add({ node: nodes_arr });
        expect(manager.nodes.length).toEqual(++nodes);
    });

});