var manager = require('../index');
var _ = require('lodash');

describe("Test LCM:", function() {
    var path = 'I:\\!Диссер\\emb6server\\exec\\emb6blank.exe';

    beforeEach(function(){
        spyOn(console, 'info');
    });

    afterAll(function(done) {
        var times = 2*manager.lines.length;
        spyOn(console, 'info');
        _.forEach(manager.nodes, function(node) {
            manager.remove({node: node});
        });
        setTimeout(function() {
            expect(console.info).toHaveBeenCalledTimes(times);
            done();
        }, 100);
    });

    it("add line 1", function() {
        var nodes = [];
        var lines = [];
        nodes.push({ mac: 'x', path: path });
        nodes.push({ mac: 'y', path: path });
        lines.push({ source: nodes[0], target: nodes[1] });
        lines.push({ source: nodes[1], target: nodes[0] });
        lines.push({ source: nodes[1], target: { mac: 'e' } });
        manager.add({ node: nodes, line: lines });
        expect(console.info).toHaveBeenCalledTimes(2);

    });

    it("add line 2", function() {
        var node = { mac: 'x' };
        var nodes = [];
        var lines = [];
        nodes.push({ mac: 'c', path: path });
        nodes.push({ mac: 'd', path: path });
        lines.push({ source: node, target: nodes[1] });
        lines.push({ source: nodes[0], target: node });
        manager.add({ node: nodes, line: lines });
        expect(console.info).toHaveBeenCalledTimes(4);
    });

    it("remove line", function() {
        var nodes = [];
        nodes.push({ mac: 'c' });
        manager.remove({ node: nodes });
        expect(console.info).toHaveBeenCalledTimes(2);
    });

    it("remove line", function() {
        var lines = [];
        lines.push({ source: { mac: 'd' }, target: { mac: 'x' } });
        manager.remove({ line: lines });
        expect(console.info).toHaveBeenCalledTimes(2);
    });

});