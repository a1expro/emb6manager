var manager = require('../index');
var _ = require('lodash');

describe("Test Lines:", function() {
    var path = 'I:\\!Диссер\\emb6server\\exec\\emb6blank.exe';

    var nodes = 0;
    var lines = 0;
    var line;

    afterAll(function(done) {
        _.forEach(manager.nodes, function(node) {
            manager.remove({node: node});
        });
        setTimeout(function() {
            done();
        }, 100);
    });

    it("add new node (a)", function() {
        manager.add({ node: { mac: 'a', path: path } });
        expect(manager.nodes.length).toEqual(++nodes);
    });

    it("add new node (b)", function() {
        manager.add({ node: { mac: 'b', y: 777, path: path } });
        expect(manager.nodes.length).toEqual(++nodes);
    });

    it("add new node (c)", function() {
        manager.add({ node: { mac: 'c', y: 33, path: path } });
        expect(manager.nodes.length).toEqual(++nodes);
    });

    it("add new line (b-a)", function() {
        line = { source: { mac: 'b' }, target: { mac: 'a' } };
        manager.add({ line:  line });
        line.source.y = 777;
        expect(manager.lines.length).toEqual(++lines);
    });

    it("add existing line (a-b)", function() {
        manager.add({ line: { source: { mac: 'a' }, target: { mac: 'b' } } });
        expect(manager.lines.length).toEqual(lines);
    });

    it("check target x", function() {
        var first = _.head(manager.lines);
        expect(first.target.x).toEqual(0);
    });

    it("check source y", function() {
        var first = _.head(manager.lines);
        expect(first.source.y).toEqual(line.source.y);
    });

    it("move node (a)", function() {
        var node = { mac: 'a', x: 555, y: 667 };
        line.target = node;
        manager.change({ node: node });
        var first = _.head(manager.lines);
        expect(first).toEqual(line);
    });

    it("add new line (b-c)", function() {
        line = { source: { mac: 'b' }, target: { mac: 'c' } };
        manager.add({ line:  line });
        expect(manager.lines.length).toEqual(++lines);
    });


});