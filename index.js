'use strict';

var Manager = require('./manager');
var LCM = require('./lcm');
var _ = require('lodash');


function Emb6Manager(options) {
    var manager = new Manager;

    manager.on('new.line', updateChannels);
    manager.on('remove.line', updateChannels);

    return (manager);
}

module.exports = Emb6Manager();


var lcm = new LCM;
function updateChannels(data, context) {

    var lines = _.concat([], data);
    var wrap = _.wrap(_.padStart, function(func, text) {
        return '_0x' + func(text, 4, '0') + '_';
    });

    function getLinks(mac) {
        var scope = [];

        var idxNode = _.findIndex(context.nodes, ['mac', mac]);
        if (-1 === idxNode) return null;

        _.forEach(context.lines, function(line) {
            if (line.source.mac === mac) {
                scope.push(line.target.mac);
            }
            if (line.target.mac === mac) {
                scope.push(line.source.mac);
            }
        });

        return scope;
    }

    function publish(mac) {
        var channel = wrap(mac);
        var channels = _.chain(getLinks(mac)).map(wrap).join('').value();
        var message = 'CMD publish ' + (channels || '_NULL_');
        lcm.publish(channel, message);
    }

    function send(line) {
        publish(line.source.mac);
        publish(line.target.mac);
    }

    _.forEach(lines, send);
}