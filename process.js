var spawn = require('child_process').spawn;
var DEFAULT_PATH = 'exec/emb6blank.exe';

// TODO: check PATH (see Collections).

function Process(options) {
    var self = this;
    var path = options.path || DEFAULT_PATH;

    this.x = options.x || 0;
    this.y = options.y || 0;
    this.mac = options.mac || 0;
    this.process = spawn(path, [this.mac]);
    Object.defineProperty(this, "process", {enumerable: false});

    /* TODO: Delete. This is for debugging */
    this.process.stdout.on('data', function(data) {
        //console.log('Data: ' + data);
    });
    this.process.on('close', function(code, signal) {
        console.log(self.mac + ': was terminated (' + signal + ').');
    });
}

module.exports = Process;

/*
*
* var args = Array.prototype.slice.call(arguments);
* return (this.server.listen.apply(this.server, args));
*
* */